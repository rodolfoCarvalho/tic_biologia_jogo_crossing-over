/* 
Atividade de Biologia
Orientadora: 
Alunos: Fernando Davilo, Rodolfo Borges
Data: 03/2015
 */


/*Variaveis Globais*/
var errorsCount, canvasImg,startFixedBar, endFixedBar,formScroll, dragableObj, newObj, aux, x, y, z, r, s, t, square, sera, mouseObjX, mouseObjY, moving, a, dd, allLett, onSquare, exercise, changes, error;

/*Funções de validação formulario*/
function sendForm()
{
    /*var Instituicao = document.getElementById("InstituicaoEscola_id").value;
    var SeuNome = document.getElementById("nomeAluno_id").value;
    var EmailAluno = document.getElementById("emailAluno_id").value;
    var CursoAno = document.getElementById("cursoAno_id").value;
    var NomeProfessor = document.getElementById("nomeProfessor_id").value;
    var EmailProfessor = document.getElementById("emailProfessor_id").value;
    
    if( Instituicao === "")
    { 
        alert("Preencha o campo Instituição/Escola.");
        document.getElementById("InstituicaoEscola_id").focus();
        return false;
    }
    if( SeuNome === "")
    {
        alert("Preencha o campo Seu Nome.");
        document.getElementById("nomeAluno_id").focus();
        return false;
    }
    if( EmailAluno === "")
    {
        alert("Preencha o campo Seu e-mail.");
        document.getElementById("emailAluno_id").focus();
        return false;
    }
    if(CursoAno === "")
    {
        alert("Preencha o campo Curso/Ano.");
        document.getElementById("cursoAno_id").focus();
        return false;
    }*/
      
    /*id="InstituicaoEscola_id"  name="Instituição/Escola:" */
    window.onscroll = null;
    var barsNode, aux1, aux2, fan;
    barsNode = getOutObjText(document.getElementsByClassName("bars"));
    /*Esconde todos os botões*/
    if(getOutObjText(barsNode[0].getElementsByClassName("letter")).length==0)
    {
        document.dataForm.submit();
        return false;
    }
    aux = barsNode.length;
    aux1 = aux-1;
    while(aux1>=0)//Mostrar todas as barras com botẽs escondidos; tirar a foto; anexar ao formulario e submeter
    {
        hideButtonTag(barsNode[aux1]);
        barsNode[aux1].parentNode.style.display = "block";
        html2canvas(barsNode[aux1], {onrendered: addImageInForm});
        aux1--;
    }
    return false;
}
function addImageInForm(canvas){
    var img;
    //inp = document.getElementById("image_exercise1");
    //window.alert(canvas.toDataURL("image/png"));
    img = document.createElement("textarea");
    img.value = canvas.toDataURL("image/png");
    img.style.display = 'none';//permanecer escondido quando for colocado
    document.dataForm.appendChild(img);
    if(aux==1)
    {
        img.name = 'image_exercise1';
        document.dataForm.submit();
    }
    else
    {
        img.name = 'image_exercise'+aux;
    }
    aux--;
}
function hideButtonTag(bar)
{
    var aux1, len;
    aux1 = getOutObjText(bar.getElementsByTagName('button'));
    len = aux1.length;
    while(len>0)
    {
        aux1[len-1].style.display = 'none';
        len--;
    }
}






/*Funções da atividade*/

function init(){
    var aux;
    z = document.getElementById('aaxy1');
    r = document.getElementById('aaxy2');
    t = document.getElementById('aaxy3');
    u = document.getElementById('aaxy4');
    s = document.getElementById('aaxy5');
    changes = new Array(20);
    error = new Array(2);
    canvasImg = new Array(2);
    moving = 0;
    exercise = 1;
    window.onmousemove = handleMouseMove;
    a=0;
    onSquare = 0;
    //startFixedBar = 390;

    startFixedBar = pageTopDistance(document.getElementById("allLett"));

    //endFixedBar = 820;

    //aux = document.getElementsByClassName("formulario")[0];
    //endFixedBar = aux.offsetTop-100;

    aux = document.getElementsByClassName("container2")[0];
    endFixedBar = startFixedBar + aux.scrollHeight - 81;

    //formScroll = 650;
    formScroll = 0;//será setado corretamente sempre no inicio da parte 2 do exercício
    errorsCount = 0;
    /*dd = allLett.cloneNode(true);
    dd.style.position = 'fixed';
    dd.style.top = '0px';
    document.getElementById('bodyMain').appendChild(dd);
    dd.style.visibility = 'hidden';
    dd.setAttribute('id','allLett2');*/
    window.onscroll = moveBar;
    //moveBar();
}

function pageTopDistance(obj){
    var distance=0;
    while(obj.tagName!='BODY')
    {
        distance += obj.offsetTop;
        obj = obj.parentNode;
    }
    return distance;
}

function playMusic(ev){
    var aux, aux2;
    aux = document.getElementById('playex');
    aux2 = document.getElementById('audioex');

    aux.innerHTML = aux2.duration;
}

function addPlayList(event){
    var mouseX, mouseY, newMenuWindow;
    mouseX = event.pageX;
    mouseY = event.pageY;
    newMenuWindow = document.getElementById('playListMenu');
    newMenuWindow.style.top = mouseY;
    newMenuWindow.style.left = mouseX;
    newMenuWindow.style.display = "block";
}

function handleMouseMove(event) {
    event = event || window.event;// IE-ism
    // event.clientX and event.clientY contain the mouse position
    x = event.pageX;
    y = event.pageY;
    /*if((y-mouseObjY)>100 && a==0)
    {
        z.innerHTML = mouseObjY+"***a**"+y;
        a=1;
    }*/
    if(moving==1)
    {
        dragableObj.style.left = parseInt(dragableObj.style.left) + (x-mouseObjX);
        dragableObj.style.top = parseInt(dragableObj.style.top) + (y-mouseObjY);
        mouseObjX = x;
        mouseObjY = y;

        /*u.innerHTML = "dragableObj.style.left: "+(dragableObj.style.left)+"***** dragableObj.style.top: "+(dragableObj.style.top);*/
    }
    /*z.innerHTML = onSquare+"***a**"+dragableObj.offsetTop;
    s.innerHTML = "<br><br><br>X: "+mouseObjY+" Y: "+y;*/
}
function colorSet(obj,aux)//objeto letra e id do objeto squareIn relacionado ao quadrado da letra
{
    aux = parseInt(aux[6]);
    if(aux==1)
        obj.style.color = '#0817CB';
    else if(aux==2)
        obj.style.color = '#CF06BC';
        
}
function colorInit (obj) {
    obj.style.color = '#000000';
}

function getOutObjText(list){
    var i, j, vect;
    vect = [];
    j = 0;
    for (i = 0; i < list.length; i++) {
        if(list[i]!="[object Text]")
        {
            vect[j] = list[i];
            j++;
        }
    }
    return vect;
}
function mouseEnter(event){
    /*if(moving==1)
    {*/
        var aux, aux2, parent;
        onSquare = 1;
        if(moving==1)
        {
            parent = event.target.parentNode.parentNode.parentNode.parentNode;
            if(parent.id!='exercise2')
                colorSet(dragableObj,event.target.id);
            aux = getOutObjText(event.target.parentNode.childNodes);
            if(aux.length>2)
            {
                aux[2].style.opacity = 0.3;
            }
            event.target.style.cursor = 'grabbing';
            event.target.style.cursor = '-webkit-grabbing';
        }
        /*square = event.target;
    }*/
}
function mouseLeave(event){
    var aux, parent;
    onSquare = 0;
    if(moving==1)
    {
        parent = event.target.parentNode.parentNode.parentNode.parentNode;
        if(parent.id!='exercise2')
            colorInit(dragableObj);
        aux = getOutObjText(event.target.parentNode.childNodes);
        if(aux.length>2)
        {
            aux[2].style.opacity = 1;
        }
        event.target.style.cursor = 'default';
    }
}
//responsalvel por arrastar as letras
function drag (event) {
    event.preventDefault();
    var i, childs, obj;
    //squaresOn();
    mouseObjX = event.pageX;//captura posicao X do mouse na tela
    mouseObjY = event.pageY;//captura posicao Y do mouse na tela
    dragableObj = event.target;//reconhece o objeto que deu inicio ao evento
    restartObj(dragableObj,1);//recoloca o objeto na posicao original mantendo a cor
    event.target.style.cursor = 'grabbing';//muda cursor para "arrastando"
    event.target.style.cursor = '-webkit-grabbing';//muda cursor para "arrastando"
    if(exercise==2 && event.target.parentNode.id!="allLett")//caso estiver no exercicio 2 e interagir com a copia 2
    {
        //colocar todos as letras abaixo dos quadrados interiores (de reconhecimento *altura 2* ) p/ reconhecer um evento "drop"
        alternLettersIndex(1);
    }
    else
    {
        dragableObj.parentNode.style.zIndex = 2;
        obj = document.getElementById("allLettIn");
        obj.style.zIndex = 3;
    }
    dragableObj.className = dragableObj.className.substring(4);//Retira mudança para amarelo durante o arrasto
    moving=1;
    /*window.alert(dragableObj);*/
}
/*Responsalvel por posicionar letras em relação ao quadrados,
recebe uma lista de objetos com todos os quadrados exteriores e a posição (1 ou 3) na qual as letras devem ser colocadas */
function alternLettersIndex(index){
    var i, childs, parent, aux, square;
    aux=0;
    parent = dragableObj.parentNode;
    square = getOutObjText(parent.parentNode.parentNode.parentNode.getElementsByClassName('squareOut'));
    for (i = square.length - 1; i >= 0; i--) {
        childs = getOutObjText(square[i].childNodes);////Caso a letra arrastada venha de um quadrado, esconde o mesmo como opção para acomodação
        if(square[i]==parent)
        {
            if (index>1)
                childs[1].style.zIndex = 2;
            else
                childs[1].style.zIndex = 0;
            aux++;  
        }
        childs[2].style.zIndex = index;
    };
}
//Responsavel por finalizar um arrasto mal sucedido, recebe o evento que disparou 
function dragEnd(event){
    if(moving==1){
        //squaresOff();
        restartObj(dragableObj,1);//recolocar o objeto arrastado na posicao inicial
        window.alert('Insira em um quadrado!');//aviso do erro
        if(exercise==2 && event.target.parentNode.id!="allLett")//caso estiver no exercicio 2 e interagindo com o mesmo
        {
            //recolocar as letras acima dos quadrados interiores (de reconhecimento)
            alternLettersIndex(3);
        }
        else
        {
            dragableObj.parentNode.style.zIndex = 3;
            obj = document.getElementById("allLettIn");
            obj.style.zIndex = 2;
        }
    }
    else
    {
        event.target.style.cursor = 'grab';
        event.target.style.cursor = '-webkit-grab';
    }
}
function restartObj(obj, keepColor){
    obj.style.left = 0;
    obj.style.top = 0;
    obj.style.cursor = 'grab';
    obj.style.cursor = '-webkit-grab';
    obj.style.opacity = 1;
    if(keepColor==0)
        colorInit(obj);
    if(obj.className.length < 14)
        obj.className = "let " + obj.className;
    moving = 0;
}
function preventDef (event) {
    event.preventDefault();
}
/*Responsalvel por caracterizar quadrados internos das letras erradas/certas do 2º exercicio
Recebe o objeto letra e o status 1 ou 0 para certo ou errado respectivamente*/
function boxStatus(letter, status)
{
    var parent, childs;
    parent = letter.parentNode;
    childs = getOutObjText(parent.childNodes);
    if(status==0)
    {
        parent.style.borderStyle = 'solid';
        parent.style.borderColor = '#000000';
        childs[0].style.backgroundColor = '#F7FE01';
        childs[0].style.opacity = 1;
    }
    else
    {
        parent.style.borderStyle = 'solid';
        parent.style.borderColor = '#7C7979';
        childs[0].style.backgroundColor = '#817B7B';
        childs[0].style.opacity = 0.3;
    }
}
/*Responsável por soltar a letra no quadrado desejado, reposicionando ou deletandoa que estava no mesmo,
recebe o objeto que engatilhou a função*/ 
function drop(event) {
    var barsNode;//declaração de variaveis locais
    if(moving==1)//verifica se existia algum objeto sendo arrastado
    {
        //armazena o objeto que contem todos os quadrados de um exercicio
        barsNode = event.target.parentNode.parentNode.parentNode.parentNode;
        if(barsNode.id=='exercise2')//Verifica se os quadrados são do exercicio 2
            dropEx2(event.target, barsNode);//chama a função de troca para o exercicio 2
        else
            dropEx1(event.target);//chama a função de substituição para o exercicio 1
        //squaresOff();
        /*window.alert(chNodes.length);*/
    }
}
/*Função responsavel por colocar/substituir letras em um quadrado,
recebe o quadrado interno (de reconhecimento) que inicio o evento para o exercicio 1*/
function dropEx1(obj){
    var child, newObj, parent, obj;
    parent = obj.parentNode;
    child = getOutObjText(parent.childNodes);
    restartObj(dragableObj,0);
    dragableObj.parentNode.style.zIndex = 3;
    if(child.length>2)
    parent.removeChild(child[2]);

    newObj = dragableObj.cloneNode(true);
    newObj.className = newObj.className.substring(4);//Retira mudança para amarelo durante o arrasto
    colorSet(newObj,obj.id);
    newObj.style.zIndex = 1;
    newObj.style.cursor = 'default';

    parent.style.borderStyle = 'solid';
    parent.style.borderColor = '#7C7979';
    obj.style.cursor = 'default';
    parent.appendChild(newObj);
    obj = document.getElementById("allLettIn");
    obj.style.zIndex = 2;
}
/*Função responsavel por trocar letras entre dois quadrados,
recebe o quadrado interno (de reconhecimento) que inicio o evento e o objeto que contem todos os quadrados do exercicio 2*/
function dropEx2(obj, barsNode){
    var ObjParent, Obj2, parent, child, i, squares, pos1, pos2;//Declaração de variaveis locais
    squares = getOutObjText(barsNode.getElementsByClassName("squareOut"));
    //reposiciona letras acima dos quadrados
    alternLettersIndex(3);
    parent = obj.parentNode;//Armazena o quadrado externo (pai) do quadrado interno iniciador do evento
    child = getOutObjText(parent.childNodes);//armazena lista de filhos do quadrado externo
    child[1].style.cursor = 'default';//retorna o cursor do quadrado interno iniciado do evento para default
    restartObj(dragableObj,1);//retorna objeto arrastado para posição inicial mantendo a cor
    Obj2 = child[2];//guarda o objeto (letra) que estava no quadrado externo como objeto 2
    restartObj(Obj2,1);//retorna objeto 2 para posição inicial mantendo a cor
    ObjParent = dragableObj.parentNode;//armazena pai do objeto arrastado
    parent.removeChild(Obj2);//remove objeto 2 do quadrado externo iniciador do evento
    ObjParent.removeChild(dragableObj);//remove objeto arrastado de seu pai
    ObjParent.appendChild(Obj2);//coloca objeto 2 como novo filho em lugar do objeto arrastado
    parent.appendChild(dragableObj);//coloca objeto arrastado como novo filho do quadrado externo iniciado do evento

    //Verifica se alguns dos quadrados estava com erro e apaga a caracterização do mesmo de errado
    /*if(child[0].style.opacity==1)
        boxStatus(dragableObj,1);
    child = getOutObjText(ObjParent.childNodes);
    if(child[0].style.opacity==1)
        boxStatus(Obj2,1);*/

    //Marca as trocas no vetor changes
    for (i = squares.length - 1; i >= 0; i--) {
        if(squares[i]==ObjParent)
            pos1 = i;
        else if(squares[i]==parent)
            pos2 = i;
    }
    child = changes[pos1];
    changes[pos1] = changes[pos2];
    changes[pos2] = child;
    
}
function exemplo()
{
    var obj;
    obj = getOutObjText(document.getElementsByClassName("hiddeAct"));
    if(obj[0].className.length>16)
        obj[0].className = obj[0].className.substring(9);
}
function atvCheck(event){
    var square, numSquares, letters, j, i, aux, idName, barsNode, parent;
    letters = 'AaBbCcDdEe';
    barsNode = getOutObjText(document.getElementsByClassName("bars"));
    /*parent = barsNode[0].parentNode.parentNode;
    if(parent.className.length < 14)
        parent.className = "hiddeAct "+parent.className;
    setTimeout("exemplo()",2000);*/
    if(event.target.parentNode.id!='exercise2')
    {
        error[0] = 0;
        square = getOutObjText(barsNode[0].getElementsByClassName("squareOut"));
        for (i =square.length-1; i >= 0; i--)
        {
            aux = getOutObjText(square[i].childNodes);
            if(aux.length<3)
                error[0]=1;
        }
        if(error[0]==0)
        {
            aux = getOutObjText(barsNode[0].childNodes);
            aux[3].style.display = 'block';//Mostrar botao etapa 2
            j = letters.length-1;
            for (i =square.length-1; i >= 0; i--)
            {
                //window.alert(square[i].childNodes[2].id + " e "+square[i].childNodes[2] + " e: " + square[i].childNodes[2].textContent);
                //window.alert("oi");
                //window.alert(square[i].childNodes.length);
                aux = getOutObjText(square[i].childNodes);
                //window.alert(aux.length);
                if(aux.length>2)
                {
                    idName = aux[2].id;
                    if(idName[0]!=letters[j])
                    {
                        square[i].style.borderColor = '#000000';
                        aux[2].style.color = '#A1AAA2';
                        if(error[0]==0)
                            error[0]=2;
                    }
                }
                if(i%2==0)
                    j--;
            }
        }
        if(error[0]==0)
        {
            exercise = 2;
            window.alert('Parabéns!!\nPasse para a próxima fase.');
            actCpy(event);
        }
        else if(error[0]==1)
            window.alert('Existem cromossomos incompletos.');
        else if(error[0]==2)
        {
            exercise = 2;
            window.alert('Distribua os alelos segundo o enunciado.');
        }
        else
            window.alert('Existem erros no projeto.');
    }
    else
    {
        error[1] = 0;
        square = getOutObjText(barsNode[1].getElementsByClassName("letter"));
        /*aux = getOutObjText(barsNode[0].getElementsByClassName("letter"));
        j = aux.length-1;*/
        for (i = square.length-1; i >= 0; i--)
        {
            if(i==2 || i==10 || i==18)
            {
                if(changes[i]!=i-1 || changes[i-1]!=i)
                {
                    error[1] = 1;
                    if(errorsCount>2)//significa que a ultima tentativa foi a 3ª ou maior, essa portanto eu mostro os erros
                    {
                        boxStatus(square[i], 0);
                        boxStatus(square[i-1], 0);
                    }
                }
                else
                    boxStatus(square[i], 1);
                i--;
            }
            else if(changes[i]!=i)
            {
                error[1] = 1;
                if(errorsCount>2)//significa que a ultima tentativa foi a 3ª ou maior, essa portanto eu mostro os erros
                    boxStatus(square[i], 0);
            }
            else
                boxStatus(square[i], 1);
        }
        if(error[1]==0)
        {
            if(error[0]==0)
            {
                aux = window.confirm('Parabéns!! Exercício finalizado sem erros.\nPreencha o formulario a seguir para receber em seu email um resumo do que você fez.');
                if(aux)
                    window.setTimeout(scrollAtv,1,window.scrollY);
            }
            else
            {
                aux = window.confirm('Exercício parcialmente correto, ainda existem erros na parte 1.\nDeseja finalizar a atividade e receber em seu email um resumo do que você fez ?');
                if(aux)
                    window.setTimeout(scrollAtv,1,window.scrollY);
            }
        }
        else if(error[1]==1)
        {
            errorsCount++;
            window.alert('Distribua os alelos segundo o enunciado 2.');
        }
    }
    
}
function scrollAtv(i){
    window.scroll(0,i);
    if(i+10<formScroll)
    {
        window.setTimeout("scrollAtv("+(i+10)+")",1);
    }
    else if(i!=formScroll)
    {
        window.setTimeout("scrollAtv("+(formScroll)+")",1);
    }
}
function compAll(event){
    var square, aux, parent, barsNode;
    barsNode = event.target.parentNode;
    if(event.charCode==99 || event.charCode==67)
    {
        square = getOutObjText(barsNode.getElementsByClassName("squareOut"));
        if(dragableObj==null)
            dragableObj = document.getElementById('Abig');
        for (i =square.length-1; i >= 0; i--)
        {
            parent = square[i];
            aux = getOutObjText(parent.childNodes);
            if(aux.length>2)
                parent.removeChild(aux[2]);
            newObj = dragableObj.cloneNode(true);
            newObj.className = newObj.className.substring(4);//Retira mudança para amarelo durante o arrasto
            colorSet(newObj,aux[1].id);
            newObj.style.zIndex = 1;
            newObj.style.cursor = 'default';
            parent.appendChild(newObj);
            parent.style.borderStyle = 'solid';
        }
    }
}
/*function changeAnimation(obj){
    if(obj.className.length < 14)
        obj.className = "let " + obj.className;
    obj.className = "let2 " + obj.className;
}*/
function getOutExercise(centerNode,ex)
{
    if(ex==1)
    {
        centerNode[0].style.display = 'none';
        centerNode[1].style.display = 'none';
        centerNode[2].style.display = 'block';
        centerNode[3].style.display = 'block';
        window.onscroll = null;
    }
    else if(ex==2)//Ainda não funciona
    {
        centerNode[2].style.display = 'none';
        centerNode[3].style.display = 'none';
        centerNode[0].style.display = 'block';
        centerNode[1].style.display = 'block';
        window.onscroll = moveBar;
    }
}
function movePageTop()
{
    changeExerciseSetScroll(window.scrollY);
}
function changeExerciseSetScroll(value)
{
    if(value-30>0)
    {
        window.scroll(0,value-30);
        setTimeout(changeExerciseSetScroll,1,value-30);
    }
    else
        window.scroll(0,0);
}
function setAnimation(obj)
{
    if(obj.className.length < 14)
        obj.className = "hiddeAct "+ obj.className;
    setTimeout(endAnimation,1600,obj);
}
function endAnimation(obj)
{
    if(obj.className.length > 14)
        obj.className = obj.className.substring(9);
}
function changeEx(event)
{
    var centerNode, aux;
    aux = 1;
    centerNode = event.target.parentNode.parentNode.parentNode;
    if(error[0]==2 && event.target.parentNode.id!='exercise2')
        aux = window.confirm("Ainda existem erros na parte 1 do exercício!\nDeseja realmente passar para a parte 2 ?");
    if(aux)
    {
        if(event.target.parentNode.id!='exercise2')
            actCpy(event);
        else
        {
            movePageTop();
            setAnimation(centerNode);
            setTimeout(getOutExercise,800, getOutObjText(centerNode.childNodes),2);
        }
        window.setTimeout(function(){formScroll = pageTopDistance(document.getElementsByClassName("formulario")[0]);},5000);
    }
}
function actCpy(event){
    var i, centerNode, barsNode, newNode, newNodeChilds, squares, parent;
    errorsCount = 0;
    barsNode = event.target.parentNode;
    //Verificar se ja existe um exercicio 2, caso sim, apaga-lo
    parent = barsNode.parentNode.parentNode;//recebe objeto 'center'
    centerNode = getOutObjText(parent.childNodes);
    newNode = getOutObjText(centerNode[3].childNodes);
    if(newNode.length>0)
        centerNode[3].removeChild(newNode[0]);
    //Criar e preparar novo filho
    newNode = barsNode.cloneNode(true);
    newNode.id = 'exercise2'
    newNodeChilds = getOutObjText(newNode.childNodes);
    newNodeChilds[newNodeChilds.length-1].innerHTML = "Voltar para parte 1 do Exercicio";
    newNode.style.top = 10;
    square = newNode.getElementsByClassName("squareOut");
    for (i = square.length - 1; i >= 0; i--) {
        square[i].style.borderColor = '#7C7979';//modifica cor de borda do squareOut
        newNodeChilds = getOutObjText(square[i].childNodes);
        colorSet(newNodeChilds[2], newNodeChilds[1].id);//modifica cor da letra
        newNodeChilds[2].style.zIndex = 3;
        newNodeChilds[2].style.cursor = 'grab';
        newNodeChilds[2].style.cursor = '-webkit-grab';
        //changeAnimation(newNodeChilds[2]);
        restartObj(newNodeChilds[2],1);
    }
    /*Preparar local e colocar novo filho*/
    //window.scroll(0,1);
    centerNode[3].style.height = 410;
    centerNode[3].appendChild(newNode);
    if(allLett!=null)
    {
        allLett.style.position = 'absolute';
        allLett.style.marginTop = 10;
    }
    movePageTop();
    setAnimation(parent);
    setTimeout(getOutExercise,800, centerNode,1);
    //Iniciar vetor da atividade dois "changes"
    for (i = changes.length - 1; i >= 0; i--) {
        changes[i] = i;
    };
    //Encerra a movimentação da barra e colocar o visor na atividade 2
    /*window.scroll(0,1188);*/
}

function dragsHand(event){
    if(event.button==0)
    {
        event.target.style.cursor = 'grabbing';
        event.target.style.cursor = '-webkit-grabbing';
    }
}
/*function comeca(event,obj){
    event.defaultPrevented;
    dragableObj = document.getElementById(obj);
    dragableObj.style.position = 'relative';
    dragableObj.style.cursor = 'grabbing';
    if(dragableObj.style.left==0)
        dragableObj.style.left = 0;
    if(dragableObj.style.top==0)
        dragableObj.style.top=0;
    mouseObjX = event.clientX;
    mouseObjY = event.clientY;
    z.innerHTML = (dragableObj.scrollLeft)+"*****"+(dragableObj.offsetTop-8);
    window.onmousemove = handleMouseMove;
    moving = 1;
}*/
function termina(event){
    event.preventDefault();
    dragableObj.style.cursor = 'grab';
    dragableObj.style.cursor = '-webkit-grab';
    moving=0;
    nova();
    //window.onmousemove = null;
}
function moveBar(){
    var obj, topDistance;
    obj = document.getElementById("allLettIn");
    topDistance = document.body.scrollTop;
    if(allLett==null)
        allLett = document.getElementById('allLett');
    if(topDistance>startFixedBar && topDistance<endFixedBar)
    {
        /*dd.style.visibility = 'visible';
        allLett.style.visibility = 'hidden';*/
        allLett.style.position = 'fixed';
        allLett.style.marginTop = 0;
        obj.style.position = 'fixed';
        obj.style.marginTop = 0;
    }
    else
    {
        /*document.getElementById('allLett').style.position = 'relative';
        document.getElementById('allLett').style.top = '10px';*/
        
        /*allLett.style.visibility = 'visible';
        dd.style.visibility = 'hidden';*/
        allLett.style.position = 'absolute';
        allLett.style.marginTop = 10;
        obj.style.position = 'absolute';
        obj.style.marginTop = 10;
    }
}
function exxtow (event) {
    dragableObj.style.position = 'relative';
    /*
    dd = document.getElementById('abig').cloneNode();
    document.getElementById('square1').appendChild(dd);*/
}
function nova(){
    z.innerHTML = "offsetLeft: "+(dragableObj.offsetLeft-8)+"***** offsetTop: "+(dragableObj.offsetTop-8);
    r.innerHTML = "scrollLeft: "+(dragableObj.scrollLeft)+"***** scrollTop: "+(document.body.scrollTop);
    t.innerHTML = "dragableObj.style.left: "+(dragableObj.style.left)+"***** dragableObj.style.top: "+(dragableObj.style.top);
}
window.onload=init;
